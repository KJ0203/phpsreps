$('body').append('<div style="" id="loadingDiv"><div class="loader">Loading...</div></div>');
$(window).on('load', function(){
  setTimeout(removeLoader, 300); //wait for page load PLUS two seconds.
});
function removeLoader(){
    $( "#loadingDiv" ).fadeOut(400, function() {
      // fadeOut complete. Remove the loading div
      $( "#loadingDiv" ).remove(); //makes page more lightweight 
  });  
}
$(document).ready(function(){
    $("#navbarDropdown").click(function(event){
        event.stopPropagation();
        $("#dropdown-menu").slideToggle("slow");
    });
    $("#dropdown-menu").on("click", function (event) {
        event.stopPropagation();
        }); 
    });
$(document).on("click", function () {
    $("#dropdown-menu").hide();
});
  
// Finalization-PC-Customization
$(document).ready(function(){
    $('#btn-product-advanced').click(function(event){
        event.stopPropagation();
        $("#advanced").slideToggle("slow");
    });
    $("#advanced").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#advanced").hide();
});

function setImage(select){
    var image = document.getElementsByName("image-swap")[0];
    image.src = select.options[select.selectedIndex].value;
}

$(document).ready(function(){
    $('#filter-dropdown-container-1').click(function(event){
        event.stopPropagation();
        $("#btn-brand").slideToggle("fast");
        $("#btn-price").hide();
    });
    $("#btn-brand").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#btn-brand").slideUp() ;
    
});

$(document).ready(function(){
    $('#filter-dropdown-container-2').click(function(event){
        event.stopPropagation();
        $("#btn-price").slideToggle("fast");
        $("#btn-brand").hide() ;
    });
    $("#btn-price").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#btn-price").slideUp() ;
});

$(document).ready(function(){
    $('#filter-dropdown-container-3').click(function(event){
        event.stopPropagation();
        $("#btn-brand-2").slideToggle("fast");
        $("#btn-price").hide();
    });
    $("#btn-brand-2").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#btn-brand-2").slideUp() ;
    
});


$(document).ready(function(){
    $('#filter-dropdown-container-4').click(function(event){
        event.stopPropagation();
        $("#btn-brand-3").slideToggle("fast");
        $("#btn-price").hide();
    });
    $("#btn-brand-3").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#btn-brand-3").slideUp() ;
    
});

// $( function() {
//     $( "#slider-range" ).slider({
//       range: true,
//       min: 0,
//       max: 500,
//       values: [ 75, 300 ],
//       slide: function( event, ui ) {
//         $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
//       }
//     });
//     $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
//       " - $" + $( "#slider-range" ).slider( "values", 1 ) );
//   } );
// Help
$(document).ready(function(){
    $('#help_1').click(function(event){
        event.stopPropagation();
        $("#help_1_1").slideToggle("slow");
    });
    $("#help_1_1").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#help_1_1").hide();
});

$(document).ready(function(){
    $('#help_2').click(function(event){
        event.stopPropagation();
            $("#help_2_2").slideToggle("slow");
    });
    $("#help_2_2").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#help_2_2").hide();
});

$(document).ready(function(){
    $('#help_3').click(function(event){
        event.stopPropagation();
            $("#help_3_3").slideToggle("slow");
    });
    $("#help_3_3").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#help_3_3").hide();
});

// About Us
$(document).ready(function(){
    $('#about_1').click(function(event){
        event.stopPropagation();
        $("#about_1_1").slideToggle("slow");
    });
    $("#about_1_1").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#about_1_1").hide();
});

$(document).ready(function(){
    $('#about_2').click(function(event){
        event.stopPropagation();
            $("#about_2_2").slideToggle("slow");
    });
    $("#about_2_2").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#about_2_2").hide();
});

$(document).ready(function(){
    $('#about_3').click(function(event){
        event.stopPropagation();
            $("#about_3_3").slideToggle("slow");
    });
    $("#about_3_3").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#about_3_3").hide();
});

$(document).ready(function(){
    $('#about_4').click(function(event){
        event.stopPropagation();
            $("#about_4_4").slideToggle("slow");
    });
    $("#about_4_4").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#about_4_4").hide();
});

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// SERVICE
$(document).ready(function(){
    $('.tab-container:first').show()
    $('.tab-navigation li:first').addClass('active');
    $('.tab-navigation li').click(function(event){
        index=$(this).index();
        $('.tab-navigation li').removeClass('active');
        $(this).addClass('active');
        $('.tab-container').hide();
        $('.tab-container').eq(index).show();
    })
});

    // CART
    jQuery(document).ready(function(){
        // This button will increment the value
        $('.product-quantity-qtyplus').click(function(e){
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');
            // Get its current value
            var currentVal = parseInt($('input[name='+fieldName+']').val());
            // If is not undefined
            if (!isNaN(currentVal)  && currentVal < 999) {
                // Increment
                $('input[name='+fieldName+']').val(currentVal + 1);
            } else {
                // Otherwise put a 0 there
                $('input[name='+fieldName+']').val(currentVal = 1);
            }
        });
        // This button will decrement the value till 0
        $(".product-quantity-qtyminus").click(function(e) {
            // Stop acting like a button
            e.preventDefault();
            // Get the field name
            fieldName = $(this).attr('field');
            // Get its current value
            var currentVal = parseInt($('input[name='+fieldName+']').val());
            // If it isn't undefined or its greater than 0
            if (!isNaN(currentVal) && currentVal > 1) {
                // Decrement one
                $('input[name='+fieldName+']').val(currentVal - 1);
            } else {
                // Otherwise put a 0 there
                $('input[name='+fieldName+']').val(currentVal = 1);
            }
        });
    });

$(function () {
    $(".checkAll").click(function () {
        $("input:checkbox").prop("checked", this.checked);
    });
    $("input:checkbox:not(.checkAll)").click(function () {
        $(".checkAll").prop("checked", $("input:checkbox:not(.checkAll):checked").length == 5);
    });
});

function selectPaymentMethod(){
	if($('#Paypal').is(':checked') || $('#COD').is(':checked')){
		/*alert("checked");*/
	}else{
		alert("Please select Payment Method");
		return false;
	}
}

// Admin Sidebar Dropdown
$(document).ready(function(){
    $('#btn-admin-sidebar').click(function(event){
        event.stopPropagation();
        $("#sidebar").slideToggle("fast");
        $("#sidebar-product").hide("slow");
        $("#sidebar-user").hide("slow");
        $("#sidebar-report").hide("slow");
    });
    $("#sidebar").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#sidebar").hide();
    
});

$(document).ready(function(){
    $('#btn-admin-sidebar-product').click(function(event){
        event.stopPropagation();
        $("#sidebar-product").slideToggle("fast");
        $("#sidebar").hide("slow");
        $("#sidebar-user").hide("slow");
        $("#sidebar-report").hide("slow");
    });
    $("#sidebar-product").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#sidebar-product").hide();
});

$(document).ready(function(){
    $('#btn-admin-sidebar-user').click(function(event){
        event.stopPropagation();
        $("#sidebar-user").slideToggle("fast");
        $("#sidebar-product").hide("slow");
        $("#sidebar-report").hide("slow");
        $("#sidebar").hide("slow");
    });
    $("#sidebar-user").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#sidebar-user").hide();
});

$(document).ready(function(){
    $('#btn-admin-sidebar-report').click(function(event){
        event.stopPropagation();
        $("#sidebar-report").slideToggle("fast");
        $("#sidebar-product").hide("slow");
        $("#sidebar-user").hide("slow");
        $("#sidebar").hide("slow");
    });
    $("#sidebar-report").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#sidebar-report").hide();
});

$(document).ready(function(){
    $('#btn-admin-sidebar-enquiry').click(function(event){
        event.stopPropagation();
        $("#sidebar-enquiry").slideToggle("fast");
        $("#sidebar-product").hide("slow");
        $("#sidebar-user").hide("slow");
        $("#sidebar").hide("slow");
    });
    $("#sidebar-enquiry").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#sidebar-enquiry").hide();
});

// Search
$(document).ready(function(){
    $('#search').click(function(event){
        event.stopPropagation();
            $("#search_search").slideToggle("fast");
    });
    $("#search_search").on("click", function (event) {
        event.stopPropagation();
    });
});
$(document).on("click", function () {
    $("#search_search").hide();
});

// Dropdown Profile
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
  }
  
  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
    if (!event.target.matches('.header-action-btn')) {
      var dropdowns = document.getElementsByClassName("dropdown-menu");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }
