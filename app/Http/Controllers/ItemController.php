<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::orderBy('item_id', 'asc')->get();
        $restock_item = DB::table('items')->where('quantity', 0)->get();
        return view('Items.index', compact('items', 'restock_item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storeData = $request->validate([
            'item_id' => 'required|max:255',
            'item_name' => 'required|max:255',
            'item_desc' => 'required|max:255',
            'item_desc' => 'required|max:255',
            'price' => 'required|max:255',
            'quantity' => 'required|max:255',
        ]);
        $storeData = $request->all();
        $items = Item::updateOrCreate(  ['item_id' => $request->item_id],
                                        ['item_id' => $request->item_id,
                                        'item_name' => $request->item_name,
                                        'category' => $request->category,
                                        'item_desc' => $request->item_desc,
                                        'price' => $request->price,
                                        'quantity' => $request->quantity,]);

        return redirect('items/create')->with('success', 'Item has successfully been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items = Item::findOrFail($id);
        return view('Items.edit', compact('items'))->with('message', 'Item has successfully been updated!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = $request->validate([
            'item_id' => 'required|max:255',
            'item_name' => 'required|max:255',
            'category' => 'required|max:255',
            'item_desc' => 'required|max:255',
            'price' => 'required|max:255',
            'quantity' => 'required|max:255',
        ]);
        Item::whereId($id)->update($updateData);
        return redirect('/items')->with('message', 'Item has successfully been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $items = Item::findOrFail($id);
        $items->delete();

        return redirect('/items')->with('delete', 'Data has been deleted');
    }
}
