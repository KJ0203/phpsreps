<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sales;
use Carbon\Carbon;
use DB;
use App\Models\Item;

class PredictionController extends Controller
{
    public function index()
    {
        $sale = Sales::all();
        $items = Item::all();
        $restock_item = DB::table('items')->where('quantity', 0)->get();
        return view('Sales.index', compact('sale', 'items', 'restock_item'));
    }

    public function getindex()
    {
        return view('sales.create', compact('items'));
    }

    //Display monthly sales report
    public function predict(Request $request) 
    {   
        $min = 95;
        $max = 115;
        $index = mt_rand($min,$max);

        $dt = Carbon::create($request['from_date']);
    
        $category = $request->category;
        $getdate = $request['from_date'];

        $sum = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->where('category', $category)
        ->sum('sales_amount');
        $first = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->first();     
        $latest = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->where('category', $category)
        ->orderBy('sold_date', 'desc')->first();
        $count = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->where('category', $category)
        ->orderBy('sold_date', 'desc')->Count();
        
        $all_sum = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->sum('sales_amount');
        $all_first = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->first();
        $all_latest = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->orderBy('sold_date', 'desc')->first();
        $all_count = DB::table('sales')
        ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
        ->orderBy('sold_date', 'desc')->Count();


        $current_month = Sales::whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])->OrderBy('sold_date')->where('category', $category)->get();
        $all_month = Sales::whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])->OrderBy('sold_date')->get();
        $decrementMonth = $dt->format('Y-m-d', strtotime($dt->subMonths(1)));
        $result = $sum * ($index/100);
        $all_result = $all_sum * ($index/100);

        $get_category = $request->category;
        return view('sales.predict', ['sale' => $current_month, 'all_month' => $all_month, 'sum' => $sum, 'first' => $first, 'latest' => $latest, 'count' => $count, 'getdate' => $getdate, 'decrementMonth' =>$decrementMonth, 'result' => $result,'all_result' => $all_result, 'category' =>$category, 'get_category' => $get_category
                    ,'all_sum' => $all_sum ,'all_first' => $all_first, 'all_latest' => $all_latest, 'all_count' => $all_count]);
    }
}