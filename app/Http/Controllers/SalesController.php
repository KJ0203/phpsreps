<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sales;
use Carbon\Carbon;
use DB;
use App\Models\Item;
use Redirect;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sale = Sales::all();
        $items = Item::all();
        $restock_item = DB::table('items')->where('quantity', 0)->get();
        return view('Sales.index', compact('sale', 'items', 'restock_item'));
    }

    public function getindex()
    {
        return view('sales.create', compact('items'));
    }

        //Display weekly sales report
        public function weekly(Request $request) 
        {   
            $dt = Carbon::create($request['from_date']);
            $storeData = $request->all();
   
            $sum = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endofWeek()])
            ->sum('sales_amount');
    
            $first = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endofWeek()])->first();
            $latest = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endofWeek()])->orderBy('sold_date', 'desc')->first();
            $count = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endofWeek()])->orderBy('sold_date', 'desc')->Count();
            $getdate = $request['from_date'];
            
            $current_week = Sales::whereBetween('sold_date', [$request['from_date'], $dt->endofWeek()])->OrderBy('sold_date')->get();
            if($request->key == "key"){
                $fileName = "Weekly_$first->sold_date/_$latest->sold_date.csv";
             
                     $headers = array(
                         "Content-type"        => "text/csv",
                         "Content-Disposition" => "attachment; filename=$fileName",
                         "Pragma"              => "no-cache",
                         "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                         "Expires"             => "0"
                     );
             
                     $columns = array('sold_id', 'sold_name', 'sold_date', 'sales_amount', 'sold_quantity');
             
                     $callback = function() use($current_week, $columns) {
                         $file = fopen('php://output', 'w');
                         fputcsv($file, $columns);
             
                         foreach ($current_week as $csv) {
                             $row['sold_id']  = $csv->sold_id;
                             $row['sold_name']    = $csv->sold_name;
                             $row['sold_date']    = $csv->sold_date;
                             $row['sales_amount']  = $csv->sales_amount;
                             $row['sold_quantity']  = $csv->sold_quantity;
             
                             fputcsv($file, array($row['sold_id'], $row['sold_name'], $row['sold_date'], $row['sales_amount'], $row['sold_quantity']));
                         }
             
                         fclose($file);
                     };
                     return response()->stream($callback, 200, $headers);
            }
            return view('sales.weekly', ['sale' => $current_week, 'sum' => $sum, 'first' => $first, 'latest' => $latest, 'count' => $count, 'getdate' => $getdate]);
        }
    
        //Display monthly sales report
        public function monthly(Request $request) 
        {   
            $dt = Carbon::create($request['from_date']);
    
            $sum = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])
            ->sum('sales_amount');
            $getdate = $request['from_date'];
    
            $first = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])->first();
            $latest = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])->orderBy('sold_date', 'desc')->first();
            $count = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])->orderBy('sold_date', 'desc')->Count();
            $current_month = Sales::whereBetween('sold_date', [$request['from_date'], $dt->endOfMonth()])->OrderBy('sold_date')->get();
    
            if($request->key == "monthkey"){
                $fileName = "Monthly_$first->sold_date/_$latest->sold_date.csv";
             
                     $headers = array(
                         "Content-type"        => "text/csv",
                         "Content-Disposition" => "attachment; filename=$fileName",
                         "Pragma"              => "no-cache",
                         "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                         "Expires"             => "0"
                     );
             
                     $columns = array('sold_id', 'sold_name', 'sold_date', 'sales_amount', 'sold_quantity');
             
                     $callback = function() use($current_month, $columns) {
                         $file = fopen('php://output', 'w');
                         fputcsv($file, $columns);
             
                         foreach ($current_month as $csv) {
                             $row['sold_id']  = $csv->sold_id;
                             $row['sold_name']    = $csv->sold_name;
                             $row['sold_date']    = $csv->sold_date;
                             $row['sales_amount']  = $csv->sales_amount;
                             $row['sold_quantity']  = $csv->sold_quantity;
             
                             fputcsv($file, array($row['sold_id'], $row['sold_name'], $row['sold_date'], $row['sales_amount'], $row['sold_quantity']));
                         }
             
                         fclose($file);
                     };
                     return response()->stream($callback, 200, $headers);
            }

            return view('sales.monthly', ['sale' => $current_month, 'sum' => $sum, 'first' => $first, 'latest' => $latest, 'count' => $count, 'getdate' => $getdate]);
        }
    
        public function compare(Request $request)
        {
            $sum = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $request['to_date']])
            ->sum('sales_amount');
            $secondsum = DB::table('sales')
            ->whereBetween('sold_date', [$request['compare_from_date'], $request['compare_to_date']])
            ->sum('sales_amount');
            $comparesum = $sum - $secondsum;
            if($sum != 0){
                $percentage = (($sum - $secondsum) / ($sum + $secondsum)/2) * 100;
            }else{
                $percentage = 0;
            }
            $getfromdate = $request['from_date'];
            $gettodate = $request['to_date'];
            $getsecondfromdate = $request['compare_from_date'];
            $getsecondtodate = $request['compare_to_date'];

            $first = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $request['to_date']])->first();
            $latest = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $request['to_date']])->orderBy('sold_date', 'desc')->first();
            $count = DB::table('sales')
            ->whereBetween('sold_date', [$request['from_date'], $request['to_date']])->orderBy('sold_date', 'desc')->Count();

            $secondfirst = DB::table('sales')
            ->whereBetween('sold_date', [$request['compare_from_date'], $request['compare_to_date']])->first();
            $secondlatest = DB::table('sales')
            ->whereBetween('sold_date', [$request['compare_from_date'], $request['compare_to_date']])->orderBy('sold_date', 'desc')->first();
            $secondcount = DB::table('sales')
            ->whereBetween('sold_date', [$request['compare_from_date'], $request['compare_to_date']])->orderBy('sold_date', 'desc')->Count();

            $firstcompare = Sales::whereBetween('sold_date', [$request['from_date'], $request['to_date']])->OrderBy('sold_date')->get();
            $secondcompare = Sales::whereBetween('sold_date', [$request['compare_from_date'], $request['compare_to_date']])->OrderBy('sold_date')->get();
            
            if($request->key == "comparekey"){
                $fileName = "Compare_First_$first->sold_date/_$latest->sold_date.csv";
             
                     $headers = array(
                         "Content-type"        => "text/csv",
                         "Content-Disposition" => "attachment; filename=$fileName",
                         "Pragma"              => "no-cache",
                         "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                         "Expires"             => "0"
                     );
             
                     $columns = array('sold_id', 'sold_name', 'sold_date', 'sales_amount', 'sold_quantity');

                     $callback = function() use($firstcompare, $columns) {
                         $file = fopen('php://output', 'w');
                         fputcsv($file, $columns);
             
                         foreach ($firstcompare as $csv) {
                             $row['sold_id']  = $csv->sold_id;
                             $row['sold_name']    = $csv->sold_name;
                             $row['sold_date']    = $csv->sold_date;
                             $row['sales_amount']  = $csv->sales_amount;
                             $row['sold_quantity']  = $csv->sold_quantity;
                             fputcsv($file, array($row['sold_id'], $row['sold_name'], $row['sold_date'], $row['sales_amount'], $row['sold_quantity']));
                         }
                         fclose($file);
                     };
                     return response()->stream($callback, 200, $headers);
            }

            if($request->secondkey == "comparesecondkey"){
                $fileName = "Compare_Second_$secondfirst->sold_date/_$secondlatest->sold_date.csv";
             
                     $headers = array(
                         "Content-type"        => "text/csv",
                         "Content-Disposition" => "attachment; filename=$fileName",
                         "Pragma"              => "no-cache",
                         "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                         "Expires"             => "0"
                     );
             
                     $second_columns = array('compare_sold_id', 'compare_sold_name', 'compare_sold_date', 'compare_sales_amount', 'compare_sold_quantity');

                     $callback = function() use($secondcompare, $second_columns) {
                         $file = fopen('php://output', 'w');
                         fputcsv($file, $second_columns);

                         foreach ($secondcompare as $scsv) {
                             $row['compare_sold_id']  = $scsv->sold_id;
                             $row['compare_sold_name']    = $scsv->sold_name;
                             $row['compare_sold_date']    = $scsv->sold_date;
                             $row['compare_sales_amount']  = $scsv->sales_amount;
                             $row['compare_sold_quantity']  = $scsv->sold_quantity;
                             $row['compare_sales_status']  = $scsv->sales_status;
 
                             fputcsv($file, array($row['compare_sold_id'], $row['compare_sold_name'], $row['compare_sold_date'], $row['compare_sales_amount'], $row['compare_sold_quantity'], $row['compare_sales_status']));
                         }
                         fclose($file);
                     };
                     return response()->stream($callback, 200, $headers);
            }

            return view('sales.compare', ['sale' => $firstcompare, 'secondsale' => $secondcompare, 'sum' => $sum, 'first' => $first, 'latest' => $latest, 
            'count' => $count, 'secondsum' => $secondsum, 'secondfirst' => $secondfirst, 'secondlatest' => $secondlatest, 'secondcount' => $secondcount, 
            'comparesum' => $comparesum, 'percentage'=>$percentage, 'getfromdate'=>$getfromdate, 'gettodate'=>$gettodate, 'getsecondfromdate'=>$getsecondfromdate,
            'getsecondtodate'=>$getsecondtodate ]);
        }
        
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Item::all();
        return view('sales.create', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $storeData = $request->validate([
            'sold_name' => 'required|max:255',
            'sold_date' => 'required|max:255',
            'sales_amount' => 'required|max:255',
        ]);
        $get_item_id = Item::all()->where('item_name', $request->sold_name)->first();
        $item_id = $get_item_id->item_id;
        $get_item_quantity = $get_item_id->quantity;
        $get_sold_quantity = $request->sold_quantity;
        $deduct_quantity = $get_item_quantity - $get_sold_quantity;
        if($request->sold_quantity > $get_item_quantity){
            return redirect('sales/create')->with('stock', 'Order Quantity is more than the Stock!');
        }

        Item::where('item_name', $request->sold_name)->update(['quantity' => $deduct_quantity]);
        $sale = Sales::create(['sold_id' => $item_id,
                            'sold_name' => $request->sold_name,
                            'category' => $request->category,
                            'sold_date' => $request->sold_date,
                            'sales_amount' => $request->sales_amount,
                            'sold_quantity' => $request->sold_quantity]);
        if($get_item_quantity == 0){
            return redirect('sales/create')->with('alert', 'Sales have successfully been added and its out of stock!'); 
        }else{
            return redirect('sales/create')->with('success', 'Sales have successfully been added!');
        }
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sales::findOrFail($id);
        $each_amount = Item::where('item_name', $sale->sold_name)->first();
        return view('Sales.edit', compact('sale', 'each_amount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateData = $request->validate([
            'sold_date' => 'required|max:255',
            'sold_quantity' => 'required|max:255',
            'sales_amount' => 'required|max:255',
        ]);
        $sale = Sales::where('created_at', $request->created_at)->first();
        $item = Item::where('item_name', $request->sold_name)->first();
        $old_quantity = $sale->sold_quantity;
        $new_quantity = $request->sold_quantity;
        if($old_quantity < $new_quantity){
            $quantity_deduct = $new_quantity - $old_quantity;
            if($item->quantity < $quantity_deduct){
                return Redirect::back()->with('stock', 'Order Quantity is more than the Stock!');
            }else{
                $sub = $item->quantity - $quantity_deduct;
                Item::where('item_name', $request->sold_name)->update(['quantity' => $sub]);
                Sales::whereId($id)->update($updateData);
                return redirect('/sales')->with('success', 'Sales have successfully been updated!');
            }
        }
        else if($old_quantity > $new_quantity){
            $quantity_deduct = $old_quantity - $new_quantity;
            $sub = $item->quantity + $quantity_deduct;
            Item::where('item_name', $request->sold_name)->update(['quantity' => $sub]);
            Sales::whereId($id)->update($updateData);
            return redirect('/sales')->with('success', 'Sales have successfully been updated!');
        }

        Sales::whereId($id)->update($updateData);
        return redirect('/sales')->with('success', 'Sales have successfully been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sales::findOrFail($id);
        $sale->delete();

        return redirect('/sales')->with('delete', 'Data has been deleted');
    }


}
