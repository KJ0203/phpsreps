@extends('layouts.special')

@section('content')

<style>
  .push-top {
    margin-top: 50px;
  }
</style>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

<div class="col-md-10">
<h4 style="margin-top:25px;text-align:left !important;padding-left: 10%;">
   <u>Compare Sales Report</u>   
</h4>
</div>

<div class="col-md-5" style="display:inline-block;text-align:left !important;">
<h5 style="margin-top:25px;text-align:left !important;">
   First 
</h5>
    <table class="table">
        <thead>
            <tr class="table-warning">
            <td>ID</td>
            <td>Sales ID</td>
            <td>Sales Date</td>
            <td>Sales Amount</td>
            </tr>
        </thead>

        <tbody>
            @foreach($sale as $sales)
            <tr>
                <td>{{$sales->id}}</td>
                <td>{{$sales->sold_id}}</td>
                <td>{{$sales->sold_date}}</td>
                <td>{{$sales->sales_amount}}</td>
                <td class="text-center">
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="col-md-5" style="display:inline-block;text-align:left !important;">
<h5 style="margin-top:25px;text-align:left !important;">
   Second  
</h5>
    <table class="table">
        <thead>
            <tr class="table-warning">
            <td>ID</td>
            <td>Sales ID</td>
            <td>Sales Date</td>
            <td>Sales Amount</td>
            </tr>
        </thead>
        <tbody>

        @foreach($secondsale as $secondsales)
        <tr>
            <td>{{$secondsales->id}}</td>
            <td>{{$secondsales->sold_id}}</td>
            <td>{{$secondsales->sold_date}}</td>
            <td>{{$secondsales->sales_amount}}</td>
            <td class="text-center">
 
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

<div class="col-md-5" style="display:inline-block;text-align:left !important;">
Summarized Report
    <table class="table">
        <thead>
            <tr class="table-warning">
                <td>Total Sales Report</td>
                <td>Sales Date</td>
                <td>Sales Amount</td>
            </tr>
        </thead>

        <tr>
            <td>{{$count ?? ''}}</td>
            <td>{{$first->sold_date ?? ''}} - {{$latest->sold_date ?? ''}}</td>
            <td>{{$sum ?? ''}}</td>
        </tr>
    </table>
</div>

<div class="col-md-5" style="display:inline-block;text-align:left !important;">
    Summarized Report
        <table class="table">
            <thead>
                <tr class="table-warning">
                    <td>Total Sales Report</td>
                    <td>Sales Date</td>
                    <td>Sales Amount</td>
                </tr>
            </thead>

            <tr>
                <td>{{$secondcount ?? ''}}</td>
                <td>{{$secondfirst->sold_date ?? ''}} - {{$secondlatest->sold_date ?? ''}}</td>
                <td>{{$secondsum ?? ''}}</td>
            </tr>
        </tbody>
    </table>
    </div>

<form method="post" action="{{ route('comparedate') }}">
    <div class="col-md-5" style="display:inline-block;text-align:left !important;">
           <div class="form-group">
                @csrf
              <label for="sales_date">From Date</label>
              <input type="date" class="form-control" name="from_date" value="{{$getfromdate ?? ''}}"/>
          </div>

          <div class="form-group">
              <label for="sales_date">To Date</label>
              <input type="date" class="form-control" name="to_date" value="{{$gettodate ?? ''}}"/>
          </div>
    </div>
    <div class="col-md-5" style="display:inline-block;text-align:left !important;">
          <div class="form-group">
              <label for="sales_date">From Date</label>
              <input type="date" class="form-control" name="compare_from_date" value="{{$getsecondfromdate ?? ''}}"/>
          </div>

          <div class="form-group">
              <label for="sales_date">To Date</label>
              <input type="date" class="form-control" name="compare_to_date" value="{{$getsecondtodate ?? ''}}"/>
          </div>
    </div>
    <div class="col-md-10" style="display:inline-block;margin-top:25px">
          <button type="submit" class="btn btn-block btn-danger">Search</button>
    <div>
</form>

@if($count != 0)
<form method="post" action="{{ route('comparedate') }}">
    @csrf
    <input type="date" class="form-control" name="from_date" value="{{$getfromdate ?? ''}}" hidden/>
    <input type="date" class="form-control" name="to_date" value="{{$gettodate ?? ''}}" hidden/>
    <input type="date" class="form-control" name="compare_from_date" value="{{$getsecondfromdate ?? ''}}" hidden/>
    <input type="date" class="form-control" name="compare_to_date" value="{{$getsecondtodate ?? ''}}" hidden/>
    <input type="text" class="form-control" name="key" value="comparekey" hidden/>
    <div style="display:inline-block;margin-top:25px;width:100%">
        <button type="submit" class="btn btn-block btn-danger">Generate First Data CSV Report</button>
    <div>
</form>
@endif

@if($secondcount != 0)
<form method="post" action="{{ route('comparedate') }}">
    @csrf
    <input type="date" class="form-control" name="from_date" value="{{$getfromdate ?? ''}}" hidden/>
    <input type="date" class="form-control" name="to_date" value="{{$gettodate ?? ''}}" hidden/>
    <input type="date" class="form-control" name="compare_from_date" value="{{$getsecondfromdate ?? ''}}" hidden/>
    <input type="date" class="form-control" name="compare_to_date" value="{{$getsecondtodate ?? ''}}" hidden/>
    <input type="text" class="form-control" name="secondkey" value="comparesecondkey" hidden/>
    <div style="display:inline-block;margin-top:25px;width:100%">
        <button type="submit" class="btn btn-block btn-danger">Generate Second Data CSV Report</button>
    <div>
</form>
@endif

<h6 style="margin-top:75px;color:#DC143C;text-align:left !important">
    <b>Compare Report Results based on date selected</b>  
</h6>
    <table class="table" style="margin-top:25px">
        <thead>
            <tr class="table-warning">
                <td>Total Sales Report</td>
                <td>First Sales Date || Second Sales Data</td>
                <td>Sales Amount</td>
                <td>Profit Percentage</td>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>{{$count ?? ''}} || {{$secondcount ?? ''}}</td>
                <td>{{$first->sold_date ?? ''}} - {{$latest->sold_date ?? ''}} || {{$secondfirst->sold_date ?? ''}} - {{$secondlatest->sold_date ?? ''}}</td>
                <td>{{$comparesum ?? ''}}</td>
                <td>{!! number_format((float)($percentage),2) ?? ''!!}%</td>
            </tr>
        </tbody>
  </table>
@endsection