@extends('layouts.app')

@section('content')




<div class="push-top">
  @if(session()->get('alert'))
    <div class="alert alert-danger">
      {{ session()->get('alert') }}  
    </div>
  @endif
</div>
<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="push-top">
  @if(session()->get('stock'))
    <div class="alert alert-warning">
      {{ session()->get('stock') }}  
    </div>
  @endif
</div>

<div class="card push-top">
  <div class="card-header">
    <h5>
      Add Item Sold
    </h5> 
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('sales.store') }}">
                @csrf
          <div class="form-group">
              <label for="duplicate">Item Name</label>
              <select class="js-example-basic-single form-control" style="width:100%" name="duplicate" id='singleSelectValueDDjQuery'>
                  <option value="" disabled selected>--Select--</option>
                @foreach($items as $item)
                  <option value="{{$item->sold_name}}" data-othervalue="{{$item->quantity}}" data-othervalue-2="{{$item->price}}" data-othervalue-3="{{$item->category}}">{{$item->item_name}}</option>
                @endforeach
              </select>
          </div>

          <input type="text" class="form-control" name="sold_name" id="hidden_sold_name" hidden/>

          <div class="form-group">
              <label for="sold_date">Sold Date</label>
              <input type="date" class="form-control" name="sold_date"/>
          </div>
              <input type="text" class="form-control" name="category" id="itemcategory" hidden/>
          <div class="form-group">
              <label for="sold_quantity">Sold Quantity</label>
              <input type="text" class="form-control" name="sold_quantity" id="totalquantity"/>
          </div>
          <div class="form-group">
              <label for="sales_amount">Quantity Left: <a href="http://127.0.0.1:8000/items" id="list_quantity"></a><a id="second_list_quantity"></a></label><br>
              <label for="sales_amount">Each Item Amount</label>
              <input readonly type="text" class="form-control" name="sales_amount" id='each'/>
          </div>
          <div class="form-group">
              <label for="sales_amount">Total Sales Amount</label>
              <input readonly type="text" class="form-control" name="sales_amount" id='textFieldValueJQ'/>
          </div>
          <button type="submit" class="btn btn-block btn-danger">Create Sales Record</button>
      </form>

      <a href="{{url('sales')}}" class="btn btn-block btn-info" style="margin-top:25px">Show Sales Record List</a>
  </div>
</div>
<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>

<script>
$("#singleSelectValueDDjQuery").on("change",function(){
        //Getting Value
        var otherValue=$(this).find('option:selected').attr('data-othervalue');
        var otherValue2=$(this).find('option:selected').attr('data-othervalue-2');
        var otherValue3=$(this).find('option:selected').attr('data-othervalue-3');
        var selValue = $("#singleSelectValueDDjQuery option:selected").text();
        var settotalValue = otherValue2 * $('#totalquantity').val()
        //Setting Value
        $("#textFieldValueJQ").val(settotalValue);
        $("#each").val(otherValue2);
        $("#second").val(otherValue2);
        $("#hidden_sold_name").val(selValue);
        $("#itemcategory").val(otherValue3);
        if(otherValue == 0){
          document.getElementById("list_quantity").innerHTML = otherValue + " (Please Restock)"; 
        }
        else if(otherValue > 0 && otherValue <= 10){
          document.getElementById("list_quantity").innerHTML = otherValue + " (Nearly Out of Stock)"; 
        }
        else{
          document.getElementById("list_quantity").innerHTML = otherValue; 
        }
    });
    </script>

<script>
$("#totalquantity").on("change",function(){
        //Getting Value
        var otherValue3=$("#singleSelectValueDDjQuery").find('option:selected').attr('data-othervalue-2');
        var settotalValue = otherValue3 * $('#totalquantity').val()
        //Setting Value
        $("#textFieldValueJQ").val(settotalValue);
    });
</script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endsection