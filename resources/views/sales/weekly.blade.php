@extends('layouts.app')

@section('content')

<style>
  .push-top {
    margin-top: 50px;
  }
</style>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

<h4 style="margin-top:25px">
   <u>Weekly Sales Report</u>   
</h4>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br/>
  @endif

  <form method="post" action="{{ route('date') }}">
      <div class="form-group">
          @csrf
          <label for="sales_date">Sales Date</label>
          <input type="date" class="form-control" name="from_date" value="{{$getdate ?? ''}}"/>
      </div>

      <button type="submit" class="btn btn-block btn-danger">Create Weekly Report</button>
  </form>

  @if($count != 0)
  <form method="post" action="{{ route('date') }}">
      <div class="form-group">
          @csrf
          <input type="date" class="form-control" name="from_date" value="{{$getdate ?? ''}}" hidden/>
      </div>
      <input type="text" class="form-control" name="key" value="key" hidden/>
      <button type="submit" class="btn btn-block btn-danger">Generate CSV File</button>
  </form>
  @endif

  <!-- Search Results -->
  <h6 style="margin-top:50px">
    Sales Record List based on date selected (Weekly-based)  
  </h6>
  <table class="table">
    <thead>
        <tr class="table-warning">
          <td>ID</td>
          <td>Sales ID</td>
          <td>Sales Date</td>
          <td>Sales Amount</td>
        </tr>
    </thead>

    <tbody>
      @foreach($sale as $sales)
      <tr>
          <td>{{$sales->id}}</td>
          <td>{{$sales->sold_id}}</td>
          <td>{{$sales->sold_date}}</td>
          <td>{{$sales->sales_amount}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

<hr class ="hr-sales">

  <!-- Report Results -->
  <h6 style="margin-top:75px;color:#DC143C">
    <b>Report Results based on date selected (Weekly-based)</b>  
  </h6>
  <table class="table" style="margin-top:25px">
      <thead>
          <tr class="table-warning">
          <td>Total ID(s)</td>
          <td>Sales Date</td>
          <td>Total Sales Amount</td>
          </tr>
      </thead>
      <tbody>
        <tr>
            <form action="">
              <td>{{$count ?? ''}}</td>
              <td>{{$first->sold_date ?? ''}} - {{$latest->sold_date ?? ''}}</td>
              <td>{{$sum ?? ''}}</td>
            </form>
        </tr>
      </tbody>
  </table>

<div>

<script>
   function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }
</script>
@endsection