@extends('layouts.app')

@section('content')

<style>
    .container {
      max-width: 450px;
    }
    .push-top {
      margin-top: 50px;
    }
</style>

<div class="push-top">
  @if(session()->get('alert'))
    <div class="alert alert-danger">
      {{ session()->get('alert') }}  
    </div>
  @endif
</div>
<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="push-top">
  @if(session()->get('stock'))
    <div class="alert alert-warning">
      {{ session()->get('stock') }}  
    </div>
  @endif
</div>

<div class="card push-top">
  <div class="card-header">
    Edit & Update
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('sales.update', $sale->id) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="sales_id">Item Name</label>
              <input type="text" class="form-control" name="sold_name" value="{{$sale->sold_name}}" disabled/>
              <input type="text" class="form-control" name="sold_name" value="{{$sale->sold_name}}" hidden/>
          </div>
          <div class="form-group">
              <label for="sales_date">Sales Date</label>
              <input type="date" class="form-control" name="sold_date" value="{{$sale->sold_date}}"/>
          </div>
          <input type="text" class="form-control" name="category" value="{{$sale->category}}" hidden/>
          <input type="text" class="form-control" name="created_at" value="{{$sale->created_at}}" hidden/>
          <div class="form-group">
              <label for="sales_amount">Sold Quantity</label>
              <input type="text" class="form-control" name="sold_quantity" value="{{$sale->sold_quantity}}" id="totalquantity"/>
          </div>
          <div class="form-group">
              <label for="sales_amount">Quantity Left: <a href="http://127.0.0.1:8000/items" id="list_quantity">{{$each_amount->quantity}}</a></label><br>
              <label for="sales_amount">Each Item Amount</label>
              <input readonly type="text" class="form-control" name="each_sales_amount" id='each' value="{{$each_amount->price}}"/>
          </div>
          <div class="form-group">
              <label for="sales_amount">Total Sales Amount</label>
              <input type="text" class="form-control" name="sales_amount" value="{{$sale->sales_amount}}" id="textFieldValueJQ" disabled/>
              <input type="text" class="form-control" name="sales_amount" value="{{$sale->sales_amount}}" id="textFieldValueJQQ" hidden/>
          </div>
          <button type="submit" class="btn btn-block btn-danger">Update Sales</button>
      </form>
  </div>
</div>

<script>
$("#totalquantity").on("change",function(){
        //Getting Value
        var settotalValue = $('#totalquantity').val() * $("#each").val()
        //Setting Value
        $("#textFieldValueJQ").val(settotalValue);
        $("#textFieldValueJQQ").val(settotalValue);
    });
    </script>
@endsection