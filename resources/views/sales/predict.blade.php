@extends('layouts.app')

@section('content')

<style>
  .push-top {
    margin-top: 50px;
  }
</style>


<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

<h4 style="margin-top:25px">
   <u>Sales Forecasting</u>   
</h4>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif

  <form method="post" action="{{ route('predict') }}">
    <div class="form-group">
        @csrf
        <label for="sales_date">Sales Date</label>
        <input type="date" class="form-control" name="from_date" value="{{$getdate ?? ''}}" required/>
    </div>

    <div class="form-group">
    <label for="category">Category</label>
        <select class="js-example-basic-single form-control" style="width:100%" name="category">
            <option value="{{$get_category}}" selected>{{$get_category}}</option>
            @if($get_category == "All")
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Medical Ointment">Medical Ointment</option>
                  <option value="Skincare Products">Skincare Products</option>
                  @elseif($get_category == "Medicine")
                  <option value="All">All</option>
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Medical Ointment">Medical Ointment</option>
                  <option value="Skincare Products">Skincare Products</option>
                  @elseif($get_category == "Medical Equipment")
                  <option value="All">All</option>
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Ointment">Medical Ointment</option>
                  <option value="Skincare Products">Skincare Products</option>
                  @elseif($get_category == "Medical Ointment")
                  <option value="All">All</option>
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Skincare Products">Skincare Products</option>
                  @else
                  <option value="All">All</option>
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Medical Ointment">Medical Ointment</option>
                  @endif
        </select>
    </div>

    <button type="submit" class="btn btn-block btn-danger">Predict Sales Amount </button>
  </form>


@if( $all_count != null)
  <!-- Report Results of previous month-->
  <h6 style="margin-top:50px;">
    Report Results of the previous month 
  </h6>
  <table class="table" style="margin-top:25px">
      <thead>
          <tr class="table-warning">
          <td>Total ID(s)</td>
          <td>Sales Date</td>
          <td style="float:right">Total Sales Amount</td>
          </tr>
      </thead>
      <tbody>      
              @if($get_category == "All")
                <tr>             
                  <td>{{$all_count ?? ''}}</td>
                  <td>{{$decrementMonth}}</td>
                  <td style="float:right">RM {{$all_sum ?? ''}}</td> 
              @elseif($get_category != "All")
                  <td>{{$count ?? ''}}</td>
                  <td>{{$decrementMonth}}</td>
                  <td style="float:right">RM {{$sum ?? ''}}</td>
                </tr>
              @endif
              
              </form>
      </tbody>
  </table>

  <hr class ="hr-sales">
    @if( $get_category != "All" && $count == null )
    <p><b> No Predicted Results can be show </b></p> 
    @else
    <!-- Report Results of prediction month-->
    <h6 style="margin-top:75px;color:#DC143C">
        <b>Prediction results of the selected month ({{$category}})</b>  
      </h6>
      <table class="table" style="margin-top:25px">
          <thead>
              <tr class="table-warning">
              <td>Sales Date</td>
              <td>Category</td>
              <td style="float:right">Predicted Total Sales Amount</td>
              </tr>
          </thead>
          <tbody>
                  @if($get_category == "All")
                    <tr>            
                      <td>{{$all_first->sold_date ?? ''}} - {{$all_latest->sold_date ?? ''}}</td>
                      <td>{{$category}}</td>
                      <td style="float:right"><b>RM {{$all_result ?? ''}}</b></td> 
                  @elseif($get_category != "All")
                      <td>{{$first->sold_date ?? ''}}</td>
                      <td>{{$category}}</td>
                      <td style="float:right"><b>RM {{$result ?? ''}}</b></td>
                    </tr>
                  @endif
          </tbody>
      </table> 
    @endif
@endif
@endsection