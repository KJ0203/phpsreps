@extends('layouts.app')

@section('content')

<style>
  .push-top {
    margin-top: 50px;
  }
</style>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>  

<div class="push-top">
  @if(session()->get('delete'))
    <div class="alert alert-danger">
      {{ session()->get('delete') }}  
    </div>
  @endif
</div>
  
<div class="push-top">
  @foreach($restock_item as $item)
      @if($item->quantity == 0)
        <div class="alert alert-info">
          Please Restock {{$item->item_name}} !
        </div>
      @endif
    @endforeach
</div>


  <table class="table" style="width:800px;">
    <thead>
        <tr class="table-warning">
          <td>ID</td>
          <td>Sales ID</td>
          <td>Sales Name</td>
          <td>Sales Date</td>
          <td>Sold Quantity</td>
          <td>Sales Amount</td>
          <td class="text-center">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($sale as $sales)
        <tr>
            <td>{{$sales->id}}</td>
            <td>{{$sales->sold_id}}</td>
            <td>{{$sales->sold_name}}</td>
            <td>{{$sales->sold_date}}</td>
            <td>{{$sales->sold_quantity}} pcs</td>
            <td>RM {{$sales->sales_amount}}</td>
            <td class="text-center">
                <a href="{{ route('sales.edit', $sales->id)}}" class="btn btn-primary btn-sm">Edit</a>
                <form action="{{ route('sales.destroy', $sales->id)}}" method="post" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection