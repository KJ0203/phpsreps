<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  
    
</head>
<body>
    <!-- Header -->
    <div class="header-custom" style="height:10%">
        <p>PHP-SReps</p>
    </div>

        <!-- Action Header/Bar -->
        <div class="header-action-container">
            
            <!-- SideNav -->
            <div class="sidenav shadow-1" style="left:1%">
                <div class="logo-container">
                    <a href="https://www.facebook.com/voon.ming.5" style="background-color:white">
                        <img src="{{ asset('images/user.png') }}" style="width:120px;height:120px;">
                        <p style="color:black">Voon Cheng Ming</p>
                    </a>
                </div>
        
                <!-- Items Dropdown-->
                <div class="admin-dropdown-container" id="btn-admin-sidebar-report">
                    <a style="border-bottom:none;width:85%">Items</a>
                    <i class="fa fa-caret-down fa-caret-down-admin"  aria-hidden="true"></i>
                </div>
                
                    <!-- Items Dropdown Content -->
                    <div id="sidebar-report" class="admin-dropdown">
                        <a href="{{url('items/create')}}">Add Items</a>   
                        <a href="{{url('items')}}">Items List</a>
                        <a href="{{url('items')}}">Items Order History</a>
                    </div>

                <!-- Sales Dropdown -->
                <div class="admin-dropdown-container" id="btn-admin-sidebar-product">
                    <a style="border-bottom:none;width:85%">Sales</a>
                    <i class="fa fa-caret-down fa-caret-down-admin"  aria-hidden="true"></i>
                </div>

                    <!-- Sales Dropdown Content -->
                    <div id="sidebar-product" class="admin-dropdown">
                        <a href="{{url('sales/create')}}"> Add Sales Record</a>
                        <a href="{{url('sales')}}"> Sales Record List</a>
                        <a href="{{url('sales.weekly')}}"> Weekly Sales Report</a>
                        <a href="{{url('sales.monthly')}}"> Monthly Sales Report</a>
                        <a href="{{url('sales')}}"> Compare Sales Report</a>
                    </div>

                <!-- Prediction -->
                <div class="admin-dropdown-container" id="btn-admin-sidebar-product">
                    <a href="{{url('sales.predict')}}" style="border-bottom:none;width:85%">Forecasting</a>
                    <i class="fa fa-caret-down fa-caret-down-admin"  aria-hidden="true"></i>
                </div>
            </div>
        </div>
        
        <div class="bodycontainer" >
            <div class="col-md-9" style="float:none;margin:auto;text-align:center">
                @yield('content')
            </div>
        </div>
    </main>
</body>
</html>
