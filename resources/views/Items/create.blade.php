@extends('layouts.app')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

<div class="card push-top">
  <div class="card-header">
    <h5>
      Add Items
    </h5>
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('items.store') }}">
          <div class="form-group">
                @csrf
              <label for="item_id">Item ID</label>
              <input type="text" class="form-control" name="item_id"/>
          </div>
          <div class="form-group">
              <label for="item_name">Item Name</label>
              <input type="text" class="form-control" name="item_name"/>
          </div>
          <div class="form-group">
              <label for="category">Category</label>
              <select class="js-example-basic-single form-control" style="width:100%" name="category">
                  <option value="" disabled selected>--Select--</option>
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Medical Equipment">Medical Ointment</option>
                  <option value="Skincare Products">Skincare Products</option>
              </select>
          </div>
          <div class="form-group">
              <label for="item_desc">Item Description</label>
              <input type="text" class="form-control" name="item_desc"/>
          </div>
          <div class="form-group">
              <label for="price">Price</label>
              <input type="text" class="form-control" name="price"/>
          </div>
          <div class="form-group">
              <label for="quantity">Quantity</label>
              <input type="text" class="form-control" name="quantity"/>
          </div>
          <button type="submit" class="btn btn-block btn-danger">Create Item</button>
      </form>   
       
          <a href="{{url('items')}}" class="btn btn-block btn-info" style="margin-top:25px">Show Item List</a>
  </div>
</div>

<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection