@extends('layouts.app')

@section('content')

<style>
  .push-top {
    margin-top: 50px;
  }
</style>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div class="push-top">
  @if(session()->get('delete'))
    <div class="alert alert-danger">
      {{ session()->get('delete') }}  
    </div>
  @endif
</div>

<div class="push-top">
  @if(session()->get('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}  
    </div>
  @endif
</div>

<div class="push-top">
  @foreach($restock_item as $item)
      @if($item->quantity == 0)
        <div class="alert alert-info">
          Please Restock {{$item->item_name}} !
        </div>
      @endif
    @endforeach
</div>

  <table class="table">
    <thead>
        <tr class="table-warning">
          <td>ID</td>
          <td>Item ID</td>
          <td>Item Name</td>
          <td>Category</td>
          <td>Item Description</td>
          <td>Price</td>
          <td>Quantity</td>
          <td class="text-center" style="width:10vw">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($items as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->item_id}}</td>
            <td>{{$item->item_name}}</td>
            <td>{{$item->category}}</td>
            <td>{{$item->item_desc}}</td>
            <td>RM {{$item->price}}</td>
            <td>{{$item->quantity}} pcs</td>
            <td class="text-center">
                <a href="{{ route('items.edit', $item->id)}}" class="btn btn-primary btn-sm">Edit</a>
                <form action="{{ route('items.destroy', $item->id)}}" method="post" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                  </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection