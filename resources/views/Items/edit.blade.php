@extends('layouts.app')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<style>
    .container {
      max-width: 450px;
    }
    .push-top {
      margin-top: 50px;
    }
</style>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

<div class="card push-top">
  <div class="card-header">
    Edit & Update
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('items.update', $items->id) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="item_id">Item ID</label>
              <input type="text" class="form-control" name="item_id" value="{{$items->item_id}}"/>
          </div>
          <div class="form-group">
              <label for="item_name">Item Name</label>
              <input type="text" class="form-control" name="item_name" value="{{$items->item_name}}"/>
          </div>
          <div class="form-group">
              <label for="category">Category</label>
              <select class="js-example-basic-single form-control" style="width:100%" name="category">
                  <option value="{{$items->category}}" selected>{{$items->category}}</option>
                  @if($items->category == "Medicine")
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Medical Ointment">Medical Ointment</option>
                  <option value="Skincare Products">Skincare Products</option>
                  @elseif($items->category == "Medical Equipment")
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Ointment">Medical Ointment</option>
                  <option value="Skincare Products">Skincare Products</option>
                  @elseif($items->category == "Medical Ointment")
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Skincare Products">Skincare Products</option>
                  @else
                  <option value="Medicine">Medicine</option>
                  <option value="Medical Equipment">Medical Equipment</option>
                  <option value="Medical Ointment">Medical Ointment</option>
                  @endif
              </select>
          </div>
          <div class="form-group">
              <label for="item_desc">Item Description</label>
              <input type="text" class="form-control" name="item_desc" value="{{$items->item_desc}}"/>
          </div>
          <div class="form-group">
              <label for="price">Price</label>
              <input type="text" class="form-control" name="price" value="{{$items->price}}"/>
          </div>
          <div class="form-group">
              <label for="quantity">Quantity</label>
              <input type="text" class="form-control" name="quantity" value="{{$items->quantity}}"/>
          </div>
          <button type="submit" class="btn btn-block btn-danger">Update Item</button>
      </form>
  </div>
</div>

<script>
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endsection