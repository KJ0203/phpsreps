@extends('layouts.app')

@section('content')

<div class="push-top">
  @if(session()->get('message'))
    <div class="alert alert-success">
      {{ session()->get('message') }}  
    </div>
  @endif
</div>

<div class="push-top">
  @if(session()->get('completed'))
    <div class="alert alert-danger">
      {{ session()->get('completed') }}  
    </div>
  @endif
</div>

<style>
  .push-top {
    margin-top: 50px;
  }
</style>

<div class="push-top">
<table class="table">
    <thead>
        <tr class="table-warning">
          <td>ID</td>
          <td>Item ID</td>
          <td>Item Name</td>
          <td>Quantity</td>
          <td class="text-center" style="width:10vw">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($items as $item)
        <tr>
            <td>{{$item->id}}</td>
            <td>{{$item->item_id}}</td>
            <td>{{$item->item_name}}</td>
            <td>{{$item->quantity}}</td>
            <td class="text-center">
                <a href="{{ route('outofstock.edit', $item->id)}}" class="btn btn-primary btn-sm">Edit</a>
                <form action="{{ route('outofstock.destroy', $item->id)}}" method="post" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                  </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div>

@endsection