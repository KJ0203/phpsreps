@extends('layouts.app')

@section('content')

<style>
    .container {
      max-width: 450px;
    }
    .push-top {
      margin-top: 50px;
    }
</style>

<div class="push-top">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>

<div class="card push-top">
  <div class="card-header">
    Restock
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('outofstock.update', $items->id) }}">
              @csrf
              @method('PATCH')
          <div class="form-group">
              <label for="quantity">Quantity</label>
              <input type="text" class="form-control" name="quantity" value="{{$items->quantity}}"/>
          </div>
          <button type="submit" class="btn btn-block btn-danger">Update Item</button>
      </form>
  </div>
</div>
@endsection