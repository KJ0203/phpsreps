<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\OutofStockController;
use App\Http\Controllers\PredictionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('items', ItemController::class);
Route::resource('sales', SalesController::class);
Route::resource('outofstock', OutofStockController::class);
Route::resource('predict', SalesController::class);

Route::get('sales.weekly', [SalesController::class, 'weekly']);
Route::post('sales.weekly', [SalesController::class, 'weekly'])->name('date');

Route::get('sales.monthly', [SalesController::class, 'monthly']);
Route::post('sales.monthly', [SalesController::class, 'monthly'])->name('monthdate');

Route::get('sales.compare', [SalesController::class, 'compare']);
Route::post('sales.compare', [SalesController::class, 'compare'])->name('comparedate');

Route::get('sales.predict', [PredictionController::class, 'predict']);
Route::post('sales.predict', [PredictionController::class, 'predict'])->name('predict');